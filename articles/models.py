### -*- coding: utf-8 -*- ###
from __future__ import unicode_literals
from django.db import models
from redactor.fields import RedactorField
from django.urls import reverse
from datetime import date


class ArticleCategory(models.Model):
    header_one = models.CharField(max_length=200, verbose_name='Заголовок')
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    keywords = models.CharField(max_length=1024, blank=True)
    description = models.CharField(max_length=1024, blank=True)
    content = RedactorField(verbose_name='Контент')

    class Meta:
        verbose_name = 'Категорию'
        verbose_name_plural = 'Категории'

    def __str__(self):  # For Python 2, use __unicode__ too
        return self.slug

    def get_absolute_url(self):
        return reverse('article_category_detail', kwargs={'category_slug': self.slug})


class Article(models.Model):
    category = models.ForeignKey(ArticleCategory)
    header_one = models.CharField(max_length=200, verbose_name='Заголовок')
    image = models.ImageField(verbose_name='Изображение', blank=True)
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    keywords = models.CharField(max_length=1024, blank=True)
    description = models.CharField(max_length=1024, blank=True)
    content = RedactorField(verbose_name='Контент')
    timestamp = models.DateTimeField(auto_now=True, verbose_name='Создано')

    class Meta:
        verbose_name = 'Статью'
        verbose_name_plural = 'Статьи'

    def __str__(self):  # For Python 2, use __unicode__ too
        return self.slug

    def get_absolute_url(self):
        return reverse('article_detail', kwargs={'article_slug': self.slug, 'category': self.category})

    def save(self, *args, **kwargs):
        try:
            this_record = Article.objects.get(id=self.id)
            if this_record.image != self.image:
                this_record.image.delete(save=False)
        except:
            pass
        super(Article, self).save(*args, **kwargs)


class News(models.Model):
    header_one = models.CharField(max_length=200, verbose_name='Заголовок')
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    image = models.ImageField(verbose_name='Изображение', blank=True, null=True)
    advt = models.TextField(max_length=250, blank=True, null=True, verbose_name='Анонс',
                            help_text='Ограничено 250 символами')
    date = models.DateField(verbose_name='Создано', default=date.today)
    keywords = models.CharField(max_length=1024, blank=True)
    description = models.CharField(max_length=1024, blank=True)
    content = RedactorField(verbose_name='Контент')

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'

    def __str__(self):  # For Python 2, use __unicode__ too
        return self.slug

    def get_absolute_url(self):
        return reverse('news_detail', kwargs={'news_slug': self.slug})

    def save(self, *args, **kwargs):
        try:
            this_record = News.objects.get(id=self.id)
            if this_record.image != self.image:
                this_record.image.delete(save=False)
        except:
            pass
        super(News, self).save(*args, **kwargs)
