from django.contrib import admin
from .models import Article, ArticleCategory, News


class ArticleCategoryAdmin(admin.ModelAdmin):
    list_display = ["header_one", "slug"]
    list_display_links = ["header_one"]
    search_fields = ["header_one"]
    prepopulated_fields = {'slug': ('header_one',)}

    class Meta:
        model = ArticleCategory


class ArticleAdmin(admin.ModelAdmin):
    list_display = ["header_one", "category", "slug", "timestamp"]
    list_display_links = ["header_one"]
    list_filter = ["timestamp"]
    search_fields = ["header_one"]
    prepopulated_fields = {'slug': ('header_one',)}

    class Meta:
        model = Article


class NewsAdmin(admin.ModelAdmin):
    list_display = ["header_one", "slug"]
    list_display_links = ["header_one"]
    search_fields = ["header_one"]
    prepopulated_fields = {'slug': ('header_one',)}

    class Meta:
        model = News


admin.site.register(Article, ArticleAdmin)
admin.site.register(ArticleCategory, ArticleCategoryAdmin)
admin.site.register(News, NewsAdmin)
