from django.conf.urls import url

from articles.views import article_detail, article_category_detail, news_detail, news_list, history_detail, history_list
from main_pages.views import all_forms


urlpatterns = [
    url(r'^category/(?P<category>[-\w]+)/(?P<article_slug>[-\w]+)/$', all_forms(article_detail), name='article_detail'),
    url(r'^category/(?P<category_slug>[-\w]+)/$', all_forms(article_category_detail), name='article_category_detail'),
    url(r'^news/(?P<news_slug>[-\w]+)/$', all_forms(news_detail), name='news_detail'),
    url(r'^news/$', all_forms(news_list), name='news_list'),
    url(r'^history/(?P<his_slug>[-\w]+)/$', all_forms(history_detail), name='history_detail'),
    url(r'^history/$', all_forms(history_list), name='history_list'),
]
