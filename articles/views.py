from django.shortcuts import render, get_object_or_404

from articles.models import Article, ArticleCategory, News
from doctor.models import Service, History


def article_detail(request, article_slug=None, category=None):
    article = get_object_or_404(Article, slug=article_slug, category__slug=category)
    context = {'article': article}
    return render(request, 'articles/article_page.html', context)


def article_category_detail(request, category_slug=None):
    article_category = get_object_or_404(ArticleCategory, slug=category_slug)
    article_list = Article.objects.filter(category=article_category)
    services = Service.objects.filter(service_category=article_category)
    context = {
        'category': article_category,
        'article_list': article_list,
        'services': services
    }
    return render(request, 'articles/article_category.html', context)


def news_detail(request, news_slug=None):
    news_page = get_object_or_404(News, slug=news_slug)
    context = {'news_page': news_page, }
    return render(request, 'articles/news_page.html', context)


def news_list(request):
    news_list_all = News.objects.all().order_by('-date')
    context = {'news_list_all': news_list_all, }
    return render(request, 'articles/news_all.html', context)


def history_detail(request, his_slug=None):
    history_page = get_object_or_404(History, slug=his_slug)
    context = {'history_page': history_page}
    return render(request, 'articles/history_page.html', context)


def history_list(request):
    history_list_all = History.objects.all()
    context = {'history_list_all': history_list_all}
    return render(request, 'articles/history_list.html', context)
