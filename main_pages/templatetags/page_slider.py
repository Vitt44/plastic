from django import template
from main_pages.models import PageSlider

register = template.Library()


@register.inclusion_tag('page_slider.html')
def get_page_slider():
    return {'page_slider': PageSlider.objects.order_by('slide_position')}
