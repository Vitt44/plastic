from django import template
from main_pages.models import MainMenu

register = template.Library()


@register.inclusion_tag('main_menu.html')
def get_main_menu(self):
    m_menus = MainMenu.objects.order_by('menu_position')
    return {'m_menus': m_menus}


@register.inclusion_tag('footer_menu.html')
def get_footer_menu():
    return {'f_menu': MainMenu.objects.order_by('menu_position')}


