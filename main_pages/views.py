from django.shortcuts import render, get_object_or_404
from django.core.mail import BadHeaderError, EmailMultiAlternatives
from django.http import HttpResponse, HttpResponseRedirect
from django.template.loader import get_template
from django.db.models import Q

from .forms import CallForm, MessageForm, FileForm

from main_pages.models import Page
from doctor.models import Doctor, Service, UnderService, Review, History
from articles.models import News


def show_page(request, page_slug):
    pages = get_object_or_404(Page, slug=page_slug)
    service_menu = Service.objects.all()
    context = {
        "page": pages,
        'service_menu': service_menu,
    }
    return render(request, 'page.html', context)


def index(request):
    doctors_list = Doctor.objects.all()
    news_list = News.objects.all().order_by('-date')
    history_list = History.objects.all()[:10]
    review_list = Review.objects.all()[:10]
    if request.method == 'POST':
        file_form = FileForm(request.POST, request.FILES)
        if file_form.is_valid():
            f_name = file_form.cleaned_data['f_name']
            f_mail = file_form.cleaned_data['f_mail']
            f_message = file_form.cleaned_data['f_message']
            f_news = file_form.cleaned_data['f_news']
            f_accept = ['f_accept']
            f_file = request.FILES.get('f_file', False)
            news = ''
            if f_news:
                news = 'Посетитель дал согласие на получение новостей'
            d = {'f_name': f_name, 'f_mail': f_mail, 'news': news, 'f_message': f_message}
            htmly = get_template('mail/file.html').render(d)
            plaintext = get_template('mail/file.txt').render(d)
            from_email = 'dolgoletie.plastic@yandex.ru'
            to_email = [from_email, ]
            subject = 'Страничная форма'
            html_content = htmly
            text_content = plaintext
            if f_accept:
                try:
                    mail = EmailMultiAlternatives(subject, text_content, from_email, to_email)
                    mail.attach_alternative(html_content, "text/html")
                    if f_file:
                        mail.attach(f_file.name, f_file.read(), f_file.content_type)
                    mail.send()
                except:
                    pass
            return HttpResponseRedirect('/')
    else:
        file_form = FileForm()

    context_dict = {
        'doctors_list': doctors_list,
        'file_form': file_form,
        'news_list': news_list,
        'history_list': history_list,
        'review_list': review_list
        }
    return render(request, "index.html", context_dict)


def all_forms(view):
    def new_view(request, *args, **kwargs):
        if request.method == 'POST':
            call_form = CallForm(request.POST)
            if call_form.is_valid():
                subject = 'Заказ звонока'
                tel = call_form.cleaned_data['tel']
                name = call_form.cleaned_data['name']
                accept = ['accept']
                d = {'tel': tel, 'name': name}
                htmly = get_template('mail/call.html').render(d)
                plaintext = get_template('mail/call.txt').render(d)
                from_email = 'dolgoletie.plastic@yandex.ru'
                to_email = [from_email, ]
                html_content = htmly
                text_content = plaintext
                if accept:
                    try:
                        msg = EmailMultiAlternatives(subject, text_content, from_email, to_email)
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                    except BadHeaderError:
                        return HttpResponse('Invalid mail send!')
                return HttpResponseRedirect('/')
        else:
            call_form = CallForm()

        if request.method == 'POST':
            message_form = MessageForm(request.POST)
            if message_form.is_valid():
                subject = 'Сообщение'
                mes_tel = message_form.cleaned_data['mes_tel']
                mes_name = message_form.cleaned_data['mes_name']
                mes_message = message_form.cleaned_data['mes_message']
                mes_accept = ['mes_accept']
                d = {'mes_tel': mes_tel, 'mes_name': mes_name, 'mes_message': mes_message}
                htmly = get_template('mail/mess.html').render(d)
                plaintext = get_template('mail/mess.txt').render(d)
                from_email = 'dolgoletie.plastic@yandex.ru'
                to_email = [from_email, ]
                html_content = htmly
                text_content = plaintext
                if mes_accept:
                    try:
                        msg = EmailMultiAlternatives(subject, text_content, from_email, to_email)
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                    except BadHeaderError:
                        return HttpResponse('Invalid mail send!')
                return HttpResponseRedirect('/')
        else:
            message_form = MessageForm()

        return view(request, *args, **kwargs)
    return new_view


def service_search(request):
    service_list = Service.objects.all()
    under_service_list = UnderService.objects.all()
    errors = []
    search = request.GET["s"]
    if not search:
        errors.append('Пустой запрос')
    else:
        service_list = service_list.filter(
            Q(header_one__icontains=search) |
            Q(content__icontains=search)
        ).distinct()
        under_service_list = under_service_list.filter(
            Q(header_one__icontains=search) |
            Q(content__icontains=search)
        ).distinct()
    context = {
        'service_list': service_list,
        'under_service_list': under_service_list,
        'errors': errors,
    }
    return render(request, "search.html", context)


def book_static_page(request):
    return render(request, 'book.html')
