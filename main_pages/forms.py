from django import forms


class CallForm(forms.Form):
    tel = forms.CharField(required=True, max_length=14)
    name = forms.CharField(required=True, max_length=50)
    accept = forms.BooleanField(required=True)


class MessageForm(forms.Form):
    mes_tel = forms.CharField(required=True, max_length=14)
    mes_name = forms.CharField(required=True, max_length=50)
    mes_message = forms.CharField(required=True, max_length=360)
    mes_accept = forms.BooleanField(required=True)


class FileForm(forms.Form):
    f_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'ФИО'}), required=True, max_length=50)
    f_mail = forms.EmailField(widget=forms.EmailInput(attrs={'placeholder': 'Email'}), required=True)
    f_message = forms.CharField(widget=forms.Textarea(attrs={'rows': '4', 'cols': '30'}), required=True, max_length=300)
    f_file = forms.FileField(widget=forms.FileInput, required=False)
    f_news = forms.BooleanField(widget=forms.CheckboxInput(check_test=lambda value: True, attrs={'value': 'a1'}), required=False)
    f_accept = forms.BooleanField(widget=forms.CheckboxInput(attrs={'value': 'a5'}), required=True)
