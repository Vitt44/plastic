### -*- coding: utf-8 -*- ###
from __future__ import unicode_literals
from django.db import models
from django.urls import reverse
from redactor.fields import RedactorField


class Page(models.Model):
    title = models.CharField(max_length=200, verbose_name='Заголовок')
    slug = models.SlugField(unique=True)
    title_page = models.CharField(max_length=300, blank=True, verbose_name='title')
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True, verbose_name='Создано')
    header_two = models.CharField(max_length=200, verbose_name='Подзаголовок', blank=True)
    content = RedactorField(verbose_name='Контент')
    keywords = models.CharField(max_length=1024, blank=True)
    description = models.CharField(max_length=1024, blank=True)
    template_two = models.BooleanField(default=True, verbose_name='Типовой шаблон',
                                       help_text='Без сайдбара и изображения')

    class Meta:
        verbose_name = 'Типовую Сраницу'
        verbose_name_plural = 'Типовые страницы'

    def __str__(self):  # For Python 2, use __unicode__ too
        return self.slug

    def get_absolute_url(self):
        return reverse('show_page', kwargs={'page_slug': self.slug})


class Slide(models.Model):
    title = models.CharField(max_length=200, verbose_name='Заголовок')
    content = RedactorField(blank=True, verbose_name='Контент')
    slide_position = models.IntegerField(verbose_name='Позиция', unique=True, default=1)
    image = models.ImageField(null=True, blank=True, verbose_name='Изображене')
    button = models.URLField(verbose_name='Ссылка', null=True, blank=True)

    class Meta:
        verbose_name = 'Слайд'
        verbose_name_plural = 'Слайдер'

    def __str__(self):  # For Python 2, use __unicode__ too
        return self.title

    def save(self, *args, **kwargs):
        try:
            this_record = Slide.objects.get(id=self.id)
            if this_record.image != self.image:
                this_record.image.delete(save=False)
        except:
            pass
        super(Slide, self).save(*args, **kwargs)


class PageSlider(models.Model):
    title = models.CharField(max_length=70, verbose_name='Заголовок')
    text = RedactorField(max_length=180, blank=True, verbose_name='Описание')
    slide_position = models.IntegerField(verbose_name='Позиция', unique=True, default=1)
    image = models.ImageField(null=True, blank=True, verbose_name='Изображене')

    class Meta:
        verbose_name = 'Страничный Слайд'
        verbose_name_plural = 'Страничный Слайдер'

    def __str__(self):  # For Python 2, use __unicode__ too
        return self.title

    def save(self, *args, **kwargs):
        try:
            this_record = PageSlider.objects.get(id=self.id)
            if this_record.image != self.image:
                this_record.image.delete(save=False)
        except:
            pass
        super(PageSlider, self).save(*args, **kwargs)


class MainMenu(models.Model):
    menu_name = models.CharField(max_length=128, verbose_name='Имя в меню')
    url = models.URLField(verbose_name='Ссылка настраницу')
    menu_position = models.IntegerField(verbose_name='Позиция в меню', unique=True, default=1)

    class Meta:
        verbose_name = 'Меню'
        verbose_name_plural = 'Меню'

    def __str__(self):  # For Python 2, use __unicode__ too
        return self.menu_name
