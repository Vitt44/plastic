from django.conf.urls import url

from .views import show_page, index, all_forms, service_search, book_static_page

urlpatterns = [
    url(r'^$', all_forms(index), name="index"),
    url(r'^book/$', book_static_page, name='book_static_page'),
    url(r'^search/$', all_forms(service_search), name='service_search'),
    url(r'^(?P<page_slug>[-\w]+)/$', all_forms(show_page), name='show_page'),
]
