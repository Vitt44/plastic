from django.contrib import admin
from main_pages.models import Page, Slide, MainMenu, PageSlider


class PageAdmin(admin.ModelAdmin):
    list_display = ["title", "timestamp", "header_two"]
    list_display_links = ["title"]
    list_filter = ["timestamp"]
    search_fields = ["title"]
    prepopulated_fields = {'slug': ('title',)}

    class Meta:
        model = Page


class SlideAdmin(admin.ModelAdmin):
    list_display = ["title", "slide_position"]
    list_display_links = ["title"]

    class Meta:
        model = Slide
        
        
class PageSliderAdmin(admin.ModelAdmin):
    list_display = ["title", "slide_position"]
    list_display_links = ["title"]

    class Meta:
        model = PageSlider


class MainMenuAdmin(admin.ModelAdmin):
    list_display = ["menu_name", "menu_position"]
    list_display_links = ["menu_name"]

    class Meta:
        model = MainMenu


admin.site.register(Page, PageAdmin)
admin.site.register(Slide, SlideAdmin)
admin.site.register(PageSlider, PageSliderAdmin)
admin.site.register(MainMenu, MainMenuAdmin)

