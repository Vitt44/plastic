from django.shortcuts import render, get_object_or_404

from doctor.models import Doctor, Service, Stock, UnderService, Review, History


def doctor_detail(request, slug=None):
    doctors = get_object_or_404(Doctor, slug=slug)
    doctor_service = Service.objects.filter(doctor=doctors)
    doctor_reviews = Review.objects.filter(doctor=doctors)
    context = {
        'doctors': doctors,
        'doctor_service': doctor_service,
        'doctor_reviews': doctor_reviews
    }
    return render(request, 'doctor/doctor.html', context)


def service_detail(request, slug=None):
    service = get_object_or_404(Service, slug=slug)
    service_stock = Stock.objects.filter(service=service)
    service_menu = Service.objects.order_by("service_category")
    under_service = UnderService.objects.filter(service=service)
    history_list = History.objects.filter(history_service=service)
    doctor_service = service.doctor.all()
    context = {
        'service': service,
        'service_stock': service_stock,
        'service_menu': service_menu,
        'under_service': under_service,
        'history_list': history_list,
        'doctor_service': doctor_service
    }
    return render(request, 'doctor/service.html', context)


def stock_list(request):
    stock_list_all = Stock.objects.all()
    service_menu = Service.objects.order_by("service_category")
    context = {
        'stock_list_all': stock_list_all,
        'service_menu': service_menu,
    }
    return render(request, 'doctor/stock_list.html', context)


def under_detail(request, under_slug=None, service=None):
    under = get_object_or_404(UnderService, slug=under_slug, service__slug=service)
    service_menu = Service.objects.all()
    under_service = UnderService.objects.all()
    service_stock = Stock.objects.filter(under_service=under)
    context = {
        'under': under,
        'under_service': under_service,
        'service_menu': service_menu,
        'service_stock': service_stock,
    }
    return render(request, 'doctor/under_service.html', context)


def doctor_list(request):
    doctors_list = Doctor.objects.all()
    context = {'doctors_list': doctors_list}
    return render(request, 'doctor/doctors_list.html', context)


def review_list(request):
    reviews_list = Review.objects.all()
    context = {'reviews_list': reviews_list}
    return render(request, 'doctor/reviews_list.html', context)


def review_detail(request, slug=None):
    review = get_object_or_404(Review, slug=slug)
    context = {'review': review}
    return render(request, 'doctor/review.html', context)

