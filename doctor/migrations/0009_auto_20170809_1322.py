# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-08-09 10:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('doctor', '0008_auto_20170714_1203'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doctor',
            name='description',
            field=models.CharField(blank=True, max_length=1024),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='keywords',
            field=models.CharField(blank=True, max_length=1024),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='name',
            field=models.CharField(max_length=200, verbose_name='Имя'),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='title',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AlterField(
            model_name='service',
            name='description',
            field=models.CharField(blank=True, max_length=1024),
        ),
        migrations.AlterField(
            model_name='service',
            name='header_two',
            field=models.CharField(blank=True, max_length=200, verbose_name='Подзаголовок'),
        ),
        migrations.AlterField(
            model_name='service',
            name='keywords',
            field=models.CharField(blank=True, max_length=1024),
        ),
        migrations.AlterField(
            model_name='service',
            name='title',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AlterField(
            model_name='underservice',
            name='description',
            field=models.CharField(blank=True, max_length=1024),
        ),
        migrations.AlterField(
            model_name='underservice',
            name='header_two',
            field=models.CharField(blank=True, max_length=200, verbose_name='Подзаголовок'),
        ),
        migrations.AlterField(
            model_name='underservice',
            name='keywords',
            field=models.CharField(blank=True, max_length=1024),
        ),
        migrations.AlterField(
            model_name='underservice',
            name='title',
            field=models.CharField(blank=True, max_length=200),
        ),
    ]
