# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-16 10:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('doctor', '0003_stock_period'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='menu_name',
            field=models.CharField(default='Услуга', help_text='Название в меню сайтбара', max_length=200, verbose_name='В Меню'),
        ),
    ]
