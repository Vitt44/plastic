# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-07-14 09:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('doctor', '0007_auto_20170616_1738'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stock',
            name='service',
            field=models.ManyToManyField(blank=True, to='doctor.Service', verbose_name='Услуги'),
        ),
        migrations.AlterField(
            model_name='stock',
            name='under_service',
            field=models.ManyToManyField(blank=True, to='doctor.UnderService', verbose_name='Подслуги'),
        ),
    ]
