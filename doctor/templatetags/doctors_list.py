from django import template
from doctor.models import Doctor

register = template.Library()


@register.inclusion_tag('doctors_list.html')
def get_doctors_list():
    return {'doctors_list': Doctor.objects.prefetch_related('special')}
