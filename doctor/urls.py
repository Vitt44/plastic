from django.conf.urls import url

from .views import doctor_detail, service_detail, stock_list, under_detail, doctor_list, review_list, review_detail
from main_pages.views import all_forms

urlpatterns = [
    url(r'^doctor/(?P<slug>[-\w]+)/$', all_forms(doctor_detail), name='doctor_detail'),
    url(r'^uslugi/(?P<slug>[-\w]+)/$', all_forms(service_detail), name='service_detail'),
    url(r'^uslugi/(?P<service>[-\w]+)/(?P<under_slug>[-\w]+)/$', all_forms(under_detail), name='under_detail'),
    url(r'^akcii/$', all_forms(stock_list), name='stock_list'),
    url(r'^vrachi/$', all_forms(doctor_list), name='doctor_list'),
    url(r'^otzivi/$', all_forms(review_list), name='review_list'),
    url(r'^otzivi/(?P<slug>[-\w]+)/$', all_forms(review_detail), name='review_detail'),
]
