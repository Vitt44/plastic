### -*- coding: utf-8 -*- ###
from __future__ import unicode_literals
from django.db import models
from redactor.fields import RedactorField
from django.urls import reverse
from datetime import date

from articles.models import ArticleCategory


class Doctor(models.Model):
    name = models.CharField(max_length=200, verbose_name='Имя')
    title = models.CharField(max_length=200, blank=True)
    slug = models.SlugField(unique=True)
    keywords = models.CharField(max_length=1024, blank=True)
    description = models.CharField(max_length=1024, blank=True)
    image = models.ImageField(null=True, blank=True, verbose_name='Фото')
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True, verbose_name='Создано')
    special = models.CharField(max_length=200, verbose_name='Специализация', blank=True, null=True)
    experience = models.CharField(max_length=200, verbose_name='Стаж', blank=True, null=True)
    category = models.CharField(max_length=200, verbose_name='Категория', blank=True, null=True)
    content = RedactorField(verbose_name='Контент')

    class Meta:
        verbose_name = 'Врача'
        verbose_name_plural = 'Врачи'

    def __str__(self):  # For Python 2, use __unicode__ too
        return self.slug

    def get_absolute_url(self):
        return reverse('doctor_detail', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        try:
            this_record = Doctor.objects.get(id=self.id)
            if this_record.image != self.image:
                this_record.image.delete(save=False)
        except:
            pass
        super(Doctor, self).save(*args, **kwargs)


class Service(models.Model):
    header_one = models.CharField(max_length=200, verbose_name='Заголовок')
    header_two = models.CharField(max_length=200, blank=True, verbose_name='Подзаголовок')
    doctor = models.ManyToManyField(Doctor, verbose_name='Врачи')
    service_category = models.ForeignKey(ArticleCategory, default=1, verbose_name='Категория')
    image = models.ImageField(verbose_name='Изображение', blank="True")
    menu_name = models.CharField(max_length=200, verbose_name='В Меню', help_text='Название в меню сайтбара',
                                 default='Услуга')
    title = models.CharField(max_length=200, blank=True)
    slug = models.SlugField(unique=True)
    keywords = models.CharField(max_length=1024, blank=True)
    description = models.CharField(max_length=1024, blank=True)
    advt = models.CharField(max_length=141, blank=True, verbose_name='Анонс', help_text='Ограничено 141 символом')
    content = RedactorField(verbose_name='Контент')

    class Meta:
        verbose_name = 'Услугу'
        verbose_name_plural = 'Услуги'

    def __str__(self):  # For Python 2, use __unicode__ too
        return self.header_one

    def get_absolute_url(self):
        return reverse('service_detail', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        try:
            this_record = Service.objects.get(id=self.id)
            if this_record.image != self.image:
                this_record.image.delete(save=False)
        except:
            pass
        super(Service, self).save(*args, **kwargs)


class Stock(models.Model):
    service = models.ManyToManyField(Service, verbose_name='Услуги', blank=True)
    #under_service = models.ManyToManyField('UnderService', blank=True, verbose_name='Подслуги')
    title = models.CharField(max_length=200, db_index=True)
    image = models.ImageField(verbose_name='Изображение', blank=True, null=True)
    period = models.DateField(verbose_name='Срок действия', default=date.today, blank=True, null=True)
    content = RedactorField(verbose_name='Описание')

    class Meta:
        verbose_name = 'Акцию'
        verbose_name_plural = 'Акции'

    def __str__(self):  # For Python 2, use __unicode__ too
        return self.title

    def save(self, *args, **kwargs):
        try:
            this_record = Stock.objects.get(id=self.id)
            if this_record.image != self.image:
                this_record.image.delete(save=False)
        except:
            pass
        super(Stock, self).save(*args, **kwargs)


class UnderService(models.Model):
    header_one = models.CharField(max_length=200, verbose_name='Заголовок')
    header_two = models.CharField(max_length=200, blank=True, verbose_name='Подзаголовок')
    service = models.ForeignKey(Service, verbose_name='Услуга')
    menu_name = models.CharField(max_length=200, verbose_name='В Меню', help_text='Название в меню сайтбара')
    title = models.CharField(max_length=200, blank=True)
    slug = models.SlugField(unique=True)
    keywords = models.CharField(max_length=1024, blank=True)
    description = models.CharField(max_length=1024, blank=True)
    content = RedactorField(verbose_name='Контент')

    class Meta:
        verbose_name = 'Подуслугу'
        verbose_name_plural = 'Подуслуги'

    def __str__(self):  # For Python 2, use __unicode__ too
        return self.header_one

    def get_absolute_url(self):
        return reverse('under_detail', kwargs={'under_slug': self.slug, 'service': self.service.slug})


class Review(models.Model):
    doctor = models.ManyToManyField(Doctor, verbose_name='Врач', blank=True)
    header = models.CharField(max_length=200, verbose_name='Заголовок')
    slug = models.SlugField(unique=True)
    name = models.CharField(max_length=200, verbose_name='Имя', blank=True)
    image = models.ImageField(verbose_name='Изображение', blank=True)
    content = RedactorField(verbose_name='Контент', blank=True)
    answer = RedactorField(verbose_name='Ответ', blank=True)
    doctor_answer = models.CharField(max_length=200, verbose_name='Имя', help_text='Имя ответчика', blank=True)
    title = models.CharField(max_length=200, blank=True)
    keywords = models.CharField(max_length=1024, blank=True)
    description = models.CharField(max_length=1024, blank=True)

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'

    def __str__(self):  # For Python 2, use __unicode__ too
        return self.header

    def get_absolute_url(self):
        return reverse('review_detail', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        try:
            this_record = Review.objects.get(id=self.id)
            if this_record.image != self.image:
                this_record.image.delete(save=False)
        except:
            pass
        super(Review, self).save(*args, **kwargs)


class History(models.Model):
    header = models.CharField(max_length=200, verbose_name='Заголовок')
    title = models.CharField(max_length=200, blank=True)
    history_service = models.ForeignKey(Service, verbose_name='Услуга')
    slug = models.SlugField(unique=True)
    image = models.ImageField(verbose_name='Изображение', blank=True)
    advt = models.TextField(max_length=300, blank=True, verbose_name='Анонс на главной',
                            help_text='Ограничено 300 символами')
    keywords = models.CharField(max_length=1024, blank=True)
    description = models.CharField(max_length=1024, blank=True)
    content = RedactorField(verbose_name='Контент')

    class Meta:
        verbose_name = 'Историю'
        verbose_name_plural = 'Истории'

    def __str__(self):  # For Python 2, use __unicode__ too
        return self.slug

    def get_absolute_url(self):
        return reverse('history_detail', kwargs={'his_slug': self.slug})

    def save(self, *args, **kwargs):
        try:
            this_record = History.objects.get(id=self.id)
            if this_record.image != self.image:
                this_record.image.delete(save=False)
        except:
            pass
        super(History, self).save(*args, **kwargs)
