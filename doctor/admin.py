from django.contrib import admin
from .models import Doctor, Service, Stock, Review, History


class DoctorAdmin(admin.ModelAdmin):
    list_display = ["name", "title", "timestamp"]
    list_display_links = ["name"]
    search_fields = ["title"]
    prepopulated_fields = {'slug': ('name',)}

    class Meta:
        model = Doctor


class ServiceAdmin(admin.ModelAdmin):
    list_display = ["header_one", "slug"]
    list_display_links = ["header_one"]
    search_fields = ["header_one"]
    prepopulated_fields = {'slug': ('header_one',)}

    class Meta:
        model = Service


class StockAdmin(admin.ModelAdmin):
    list_display = ["title"]
    list_display_links = ["title"]
    search_fields = ["title"]

    class Meta:
        model = Stock

'''
class UnderServiceAdmin(admin.ModelAdmin):
    list_display = ["header_one", "service", "slug"]
    list_display_links = ["header_one"]
    search_fields = ["header_one"]
    prepopulated_fields = {'slug': ('header_one',)}

    class Meta:
        model = UnderService
'''


class ReviewAdmin(admin.ModelAdmin):
    list_display = ["header", "name"]
    list_display_links = ["header"]
    search_fields = ["header", "name"]
    prepopulated_fields = {'slug': ('header',)}

    class Meta:
        model = Review


class HistoryAdmin(admin.ModelAdmin):
    list_display = ["header", "slug"]
    list_display_links = ["header"]
    search_fields = ["header"]
    prepopulated_fields = {'slug': ('header',)}

    class Meta:
        model = History

admin.site.register(Doctor, DoctorAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(Stock, StockAdmin)
#admin.site.register(UnderService, UnderServiceAdmin)
admin.site.register(Review, ReviewAdmin)
admin.site.register(History, HistoryAdmin)

